import React, { Component } from 'react'

 class GoogleAuth extends Component {
    state = { isSignedIn: null };

    componentDidMount() {;
        const _onGoogleLoad = () => {
            window.gapi.load('client:auth2', () => {
                window.gapi.client.init({
                    clientId: '763245707388-a6nooblkl86huofp73043ku5dcq42v1e.apps.googleusercontent.com',
                    scope: 'email'
                });
            }).then( () => {
                this.auth = window.gapi.auth2.getAuthInstance();
                this.setState({ isSignedIn: this.auth.isSignedIn.get() });
            } )
        }

        _onGoogleLoad();
    }

    renderAuthButton() {
        if (this.state.isSignedIn === null) {
            return <div>I don't know</div>
        } else if (this.state.isSignedIn) {
            return <div>I am signed in</div>
        } else {
            return <div>I am not signed in</div>
        }
    }

    render() {
        return (
            <div className="item">
               {this.renderAuthButton()}
            </div>
        )
    }
}

export default GoogleAuth;

// CLient Id
// 763245707388 - a6nooblkl86huofp73043ku5dcq42v1e.apps.googleusercontent.com

// CLient secret
// D2FGAK4UmZfOh2tdkzl7nUpp
